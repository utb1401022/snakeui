using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public partial class Arena : Node2D
{
	private readonly Random generator = new Random();
	private int width = 32;
	private int height = 16;
	private int score = 5;
	private bool alive = true;
	private Snake snake;
	private Pixel berry;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		snake = new Snake(width / 2, height / 2, Direction.Right);
		berry = GetRandomPixel((0, width), (0, height));
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if (Engine.GetPhysicsFrames() % 6 == 0)
		{
			if (alive)
			{
				alive = !snake.IsDead(width, height);

				if (berry.IsSameAs(snake.Head))
				{
					score++;
					berry = GetRandomPixel((1, width - 2), (1, height - 2));
				}

				DrawScene(snake, berry);
				ProgressGameOneStep(snake);			
			}	
		}
	}
	
	private void DrawScene(Snake snake, Pixel berry)
	{

		// snake
		var headPanel = GetNode<Panel>("Head");
		headPanel.Position = new Vector2(snake.Head.X * 10, snake.Head.Y * 10);
		
		foreach (var panel in snake.TailPanels)
		{
			panel.QueueFree();
		}
		snake.TailPanels.Clear();
		foreach (var pixel in snake.Tail)
		{
			Panel newPanel = new Panel();
			newPanel.Size = new Vector2(10, 10);
			newPanel.Position = new Vector2(pixel.X * 10, pixel.Y * 10);
			StyleBoxFlat styleBox = new StyleBoxFlat();
			styleBox.BgColor = new Color(0, 1, 0);
			newPanel.AddThemeStyleboxOverride("panel", styleBox);
			AddChild(newPanel);
			snake.TailPanels.Add(newPanel);
		}		
		
		// berry
		var berryPanel = GetNode<Panel>("Berry");
		berryPanel.Position = new Vector2(berry.X * 10, berry.Y * 10);
	}
	
	private void ProgressGameOneStep(Snake snake)
	{
		if (Input.IsActionPressed("ui_up"))
		{
			snake.Direction = CalculateNewDirection(Direction.Up, snake.Direction);
		}
		if (Input.IsActionPressed("ui_down"))
		{
			snake.Direction = CalculateNewDirection(Direction.Down, snake.Direction);
		}
		
		if (Input.IsActionPressed("ui_left"))
		{
			snake.Direction = CalculateNewDirection(Direction.Left, snake.Direction);
		}
		
		if (Input.IsActionPressed("ui_right"))
		{
			snake.Direction = CalculateNewDirection(Direction.Right, snake.Direction);
		}
				
		snake.Tail.Add(new Pixel() 
		{ 
			X = snake.Head.X, 
			Y = snake.Head.Y 
		});

		switch (snake.Direction)
		{
			case Direction.Up:
				snake.Head.Y--;
				break;
			case Direction.Down:
				snake.Head.Y++;
				break;
			case Direction.Left:
				snake.Head.X--;
				break;
			case Direction.Right:
				snake.Head.X++;
				break;
		}
		
		if (snake.Tail.Count() > score)
		{
			snake.Tail.RemoveAt(0);
		}
	}

	private Direction CalculateNewDirection(Direction future, Direction past)
	{
		return (future, past) switch
		{
			(Direction.Up, Direction.Left) => Direction.Up,
			(Direction.Up, Direction.Right) => Direction.Up,
			(Direction.Down, Direction.Left) => Direction.Down,
			(Direction.Down, Direction.Right) => Direction.Down,
			(Direction.Left, Direction.Up) => Direction.Left,
			(Direction.Left, Direction.Down) => Direction.Left,
			(Direction.Right, Direction.Up) => Direction.Right,
			(Direction.Right, Direction.Down) => Direction.Right,
			_ => past
		};
	}

		
	private Pixel GetRandomPixel((int, int) xRange, (int, int) yRange)
	{
		return new Pixel()
		{ 
			X = generator.Next(xRange.Item1, xRange.Item2), 
			Y = generator.Next(yRange.Item1, yRange.Item2)
		};
	}

}

public record Pixel
{
	public int X { get; set; }
	public int Y { get; set; }

	public bool IsSameAs(Pixel pixel)
	{
		return X == pixel.X && Y == pixel.Y;
	}
}

public enum Direction
{
	Up,
	Down,
	Left,
	Right
}

public class Snake
{
	public Pixel Head { get; set; }

	public List<Pixel> Tail { get; set; }
	
	public List<Panel> TailPanels { get; set; }

	public Direction Direction { get; set; }

	public Snake(int x, int y, Direction direction)
	{
		Head = new Pixel
		{
			X = x,
			Y = y
		};
		Direction = direction;
		Tail = new List<Pixel>(); 
		TailPanels = new List<Panel>();               
	}

	public bool IsDead(int width, int height)
	{
		return Head.X < 1 || Head.X == width || Head.Y == height || Head.Y < 1 || Tail.Any(pixel => pixel.IsSameAs(Head));
	}
}

